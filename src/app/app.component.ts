import { Component, Input } from '@angular/core';
import { CalendarOptions, DateSelectArg } from '@fullcalendar/angular';
import { format } from 'date-fns'
import { ALL, ANDREJ, MARIA, TOBI } from './MOCKS';
import deLocale from '@fullcalendar/core/locales/de'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  resources: any[]; 
  filters: any[];
  legend: any[];
  selectedResource: string = 'All';

  constructor() {
    this.resources = [
      { name: 'Tobi Techniker' },
      { name: 'Andrej Techniker'},
      { name: 'Maria Techniker'}
    ]
    this.filters = [
      { name: 'Organization' },
      { name: 'Event 1'}
    ]
    this.legend = [
      { name: 'Working Hours', class: 'working-hours-div' }
    ]
  }

  calendarOptions: CalendarOptions = {
    locale: deLocale,
    initialView: 'timeGridWeek',
    weekends: false,
    slotMinTime: '06:00:00', // start hour
    slotMaxTime: '19:00:00', // end hour
    editable: true, // resizing, drag and drop
    selectable: true, // if can be clickable
    height: "auto", // removes white space on the bottom
    nowIndicator: true,
    slotDuration: '01:00:00', 
    // slotLabelFormat: {
    //   hour: 'numeric',
    //   meridiem: 'short',
    //   omitZeroMinute: true,
    //   hour12: false // removes am/pm
    // },
    firstDay: 1, // starts on Monday
    headerToolbar: {
      left: 'prev',
      center: 'title',
      right: 'next'
    },
    titleFormat: {
      year: 'numeric',
      month: 'long'
    },
    eventTimeFormat: {
      hour: 'numeric',
      minute: '2-digit',
      hour12: false,
    },
    slotLabelContent(hookProps) {
      let slotLabel;
      if (hookProps.date.getHours() < 10) {
        slotLabel = '0' + String(hookProps.date.getHours());
      } else {
        slotLabel = String(hookProps.date.getHours());
      }
      return { html: slotLabel };
    },
    dayHeaderContent(hookProps) {
      let generalDiv = document.createElement('div');
      generalDiv.style.display = 'flex';
      generalDiv.style.flexDirection = 'column';
      let div1 = document.createElement('div')
      div1.style.fontSize = '18px';
      let div2 = document.createElement('div')
      div2.style.fontWeight = 'normal';
      div2.style.color = '#cccccc';

      div1.innerHTML = String(hookProps.date.getDate());
      div2.innerHTML = hookProps.text[0];

      generalDiv.appendChild(div1);
      generalDiv.appendChild(div2);

      let arrayOfDomNodes = [ generalDiv ]
      return { domNodes: arrayOfDomNodes }     
    },
    businessHours: [
      {
        daysOfWeek: [1, 2],
        startTime: '06:00',
        endTime: '12:00',
        display: 'background',
      },
    ],
    select: this.selectDate.bind(this),
    eventClick: this.eventClick.bind(this),
    allDaySlot: false, // there can be a slot at the top for all day events
    slotEventOverlap: false,
    events: ALL,
    eventContent(hookProps) {   
      let generalDiv = document.createElement('div');
      generalDiv.classList.add('event-content');

      let div1 = document.createElement('div');
      let div2 = document.createElement('div');

      let timeText = document.createElement('div')
      timeText.className = "fc-event-time";
      timeText.innerHTML = hookProps.timeText;

      let additionalText = document.createElement('div')
      additionalText.className = "fc-event-title";
      additionalText.innerHTML = hookProps.event.title;

      if (hookProps.event.display == 'auto') { // 'if' will be not needed if we don't use background event
        let buttonDiv = document.createElement('div');
        buttonDiv.classList.add('button-div');
        
        let deleteIcon = document.createElement('button')
        deleteIcon.className = 'pi pi-trash';
        deleteIcon.classList.add('icon');
 
        let serviceIcon = document.createElement('button')
        serviceIcon.className = 'pi pi-cog';
        serviceIcon.classList.add('icon');

        deleteIcon.addEventListener('click', function (e: Event) {
          e.stopPropagation();
          alert('delete')
        })

        serviceIcon.addEventListener('click', function (e: Event) {
          e.stopPropagation();
          alert('edit')
        })

        buttonDiv.appendChild(deleteIcon);
        buttonDiv.appendChild(serviceIcon);

        div2.appendChild(buttonDiv);
      }
      
      div1.appendChild(timeText);
      div1.appendChild(additionalText);


      generalDiv.appendChild(div1);
      generalDiv.appendChild(div2);

      let arrayOfDomNodes = [ generalDiv ]
      return { domNodes: arrayOfDomNodes } 
    }
  }

  eventClick(): void {
    alert('event clicked!')
  }

  changeEvents(event: any): void {
    console.log(event)
    this.selectedResource = event;
    if (event === 'Tobi Techniker') {
      this.calendarOptions.events = TOBI;
    } else if (event == 'Andrej Techniker') {
      this.calendarOptions.events = ANDREJ;
    } else if (event == 'Maria Techniker') {
      this.calendarOptions.events = MARIA;
    } else if (event == 'All') {
      this.calendarOptions.events = ALL;
    }
  }

  selectDate(selectInfo: DateSelectArg): void {
    alert('start date ' + format(selectInfo.start, 'P'))
  }
}
