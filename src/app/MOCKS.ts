import { EventSourceInput } from "@fullcalendar/core";

export const TOBI: EventSourceInput = [
    {
        title: 'Simple event', 
        start: '2022-08-30T08:30:00', 
        end: '2022-08-30T12:00:00', 
        color: '#518AC9'
      },
      {
        title: 'Title of event 1', 
        start: '2022-09-01T09:30:00', 
        end: '2022-09-01T12:00:00', 
        color: '#518AC9'
      },
      {
        title: 'Title of event 2', 
        start: '2022-08-29T10:00:00', 
        end: '2022-08-29T15:00:00', 
        color: '#B2FEFD', 
        textColor: '#000'
      },
      {
        title: 'All day event', 
        start: '2022-09-02T00:00:00', 
        end: '2022-09-02T23:59:59', 
        color: '#fff',
        borderColor: '#eb34ae',
        textColor: '#000'
        //allDay: true
      }
]

export const ANDREJ: EventSourceInput = [
    {
        title: 'Title of event 1', 
        start: '2022-08-31T09:30:00', 
        end: '2022-08-31T12:00:00', 
        color: '#518AC9'
      },
      {
        title: 'Title of event 2', 
        start: '2022-08-31T10:00:00', 
        end: '2022-08-31T15:00:00', 
        color: '#B2FEFD', 
        textColor: '#000'
      },
      {
        title: 'All day event', 
        start: '2022-09-02T00:00:00', 
        end: '2022-09-02T23:59:59', 
        color: '#fff',
        borderColor: '#eb34ae',
        textColor: '#000'
        //allDay: true
      }
]

export const MARIA: EventSourceInput = [
    {
        title: 'Title of event 1', 
        start: '2022-08-29T09:30:00', 
        end: '2022-08-29T12:00:00', 
        color: '#518AC9'
    },
    {
        title: 'Title of event 2', 
        start: '2022-08-31T08:00:00', 
        end: '2022-08-31T15:00:00', 
        color: '#B2FEFD', 
        textColor: '#000'
    },
    {
        title: 'All day event', 
        start: '2022-09-02T00:00:00', 
        end: '2022-09-02T23:59:59', 
        color: '#fff',
        borderColor: '#eb34ae',
        textColor: '#000'
        //allDay: true
      }
]

export const ALL: EventSourceInput = [
    {
        title: 'All day event', 
        start: '2022-09-13T00:00:00', 
        end: '2022-09-13T23:59:00', 
        color: '#fff',
        borderColor: '#eb34ae',
        textColor: '#000'
        // allDay: true
    },
    {
      title: 'Simple event', 
      start: '2022-09-14T08:30:00', 
      end: '2022-09-14T12:00:00', 
      color: '#518AC9'
    },
    {
      title: 'Title of event 1', 
      start: '2022-09-15T09:30:00', 
      end: '2022-09-15T12:00:00', 
      color: '#CDE4EC',
      borderColor: '#CDE4EC',
      textColor: '#000'
    },
    // SOLUTION 1
    // background event can be created like any other event, but there are some problems:
    // - it inherits the styling from fc-event class (rounded corners)
    // - background event has some opacity build in
    {
      start: '2022-09-15T08:00:00',
      end: '2022-09-15T16:00:00',
      display: 'background',
      color: '#D8D8D8'
    }
]

